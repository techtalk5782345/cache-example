# Hello World Cache Example

This is a simple hello world cache example using Java and Spring Boot.

## Overview

This project demonstrates the basic setup of a caching mechanism using Spring Boot and Redis.

## Prerequisites

Make sure you have the following installed:

- Java (version 8 or higher)
- Maven

## Getting Started

1. Clone the repository:

    ```
    git clone https://gitlab.com/techtalk5782345/cache-example.git
    ```

2. Navigate to the project directory:

    ```
    cd cache-example
    ```

3. Build the project:

    ```
    mvn clean install
    ```

4. Run the application:

    ```
    mvn spring-boot:run
    ```

The application will start at `http://localhost:8080`. Open this URL in your browser, and you should see a simple hello world message.

## Caching

The application uses Redis as the caching provider. The `HelloWorldService` class has a method `getHelloWorldMessage` that is annotated with `@Cacheable`. This means that the result of this method will be cached based on its parameters.

```java
@Service
public class HelloService {

   @Cacheable("helloCache")
   public String getHelloMessage(String name) {
      // Simulate a time-consuming operation
      try {
         Thread.sleep(2000);
      } catch (InterruptedException e) {
         e.printStackTrace();
      }
      return "Hello, " + name + "!";
   }
}
