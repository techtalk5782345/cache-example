package com.techtalk.cacheexample.service;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class HelloService {

    @Cacheable("helloCache")
    public String getHelloMessage(String name) {
        // Simulate a time-consuming operation
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "Hello, " + name + "!";
    }
}